package strategyDesignPatterns;

public class PersonalVehicleStrategy implements StrategyToTravel 
{

	@Override
	public void waysToTravel() 
	{
		System.out.println("Travel by Personal Vehicle");		
	}

}
