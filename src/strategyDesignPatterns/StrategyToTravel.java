package strategyDesignPatterns;

public interface StrategyToTravel 
{
	public void waysToTravel();
}
