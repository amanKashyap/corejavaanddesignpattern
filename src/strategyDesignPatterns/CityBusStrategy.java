package strategyDesignPatterns;

public class CityBusStrategy implements StrategyToTravel 
{

	@Override
	public void waysToTravel() 
	{
		System.out.println("Travel by City Bus");		
	}

}
