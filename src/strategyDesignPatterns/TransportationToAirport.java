package strategyDesignPatterns;

public class TransportationToAirport 
{	
	private StrategyToTravel strategyToTravelToAirport;
	private String customerName;
	
	public TransportationToAirport(String customerName) 
	{
		super();
		this.customerName = customerName;
	}
	
	public StrategyToTravel getStrategyToTravelToAirport() 
	{
		return strategyToTravelToAirport;
	}
	public void setStrategyToTravelToAirport(StrategyToTravel strategyToTravelToAirport) 
	{
		this.strategyToTravelToAirport = strategyToTravelToAirport;
	}
	
	public String getCustomerName() 
	{
		return customerName;
	}
	
	public void setCustomerName(String customerName) 
	{
		this.customerName = customerName;
	}
	
	public void travel()
	{
		System.out.println(this.customerName);
		this.strategyToTravelToAirport.waysToTravel();
	}
	public static void main(String[] args)
	{
		TransportationToAirport travelToAirport1 = new TransportationToAirport("customer 1");
		travelToAirport1.setStrategyToTravelToAirport(new CabStrategy());
		travelToAirport1.travel();
		
		TransportationToAirport travelToAirport2 = new TransportationToAirport("customer 2");
		travelToAirport2.setStrategyToTravelToAirport(new CityBusStrategy());
		travelToAirport2.travel();
		
		TransportationToAirport travelToAirport3 = new TransportationToAirport("customer 3");
		travelToAirport3.setStrategyToTravelToAirport(new PersonalVehicleStrategy());
		travelToAirport3.travel();
	}
	
}
