package garbageCollection;

public class FinalizeAndGC 
{
	public static void main(String[] args) 
	{
		Student s;
		for (int i =0; i<1000000 ;i++)
		{
			s= new Student(i);
		}
		System.gc();
	}
}

class Student
{
	private int studentId;

	public Student(int studentId) 
	{
		this.studentId = studentId;
	}
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	
	@Override
	public void finalize()
	{
		System.out.println("Student Id :"+studentId +" got garbage collected");
	}
	
}
