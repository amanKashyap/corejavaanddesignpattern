package innerClass;

public class RegularInnerClass 
{
	private int x = 10;
	private static int y = 20;
	/*
	 * Inside Inner Class we cannot define static methods.
	 */
	private final class InnerClass
	{
		private int x = 20;
		
		/*
		 * this keyword refers to current Inner Class instance.
		 */
		public void methodOne()
		{
			System.out.println("Inner Class : methodOne");
			System.out.println("Value of X from InnerClass: "+this.x);
			System.out.println("Value of Y from InnerClass: "+y);		
			System.out.println("Value of X from OuterClass: "+RegularInnerClass.this.x);
		}
	}
	
	public void methodOuter()
	{
		System.out.println("Outer Class : methodOuter");
		System.out.println("Value of X from OuterClass: "+this.x);
		InnerClass innerClass = new InnerClass();
		innerClass.methodOne();
	}
	public static void main(String[] args) 
	{
		RegularInnerClass outerClass = new RegularInnerClass();
		outerClass.methodOuter();
		System.out.println("*******************************************************");
		outerClass.new InnerClass().methodOne();

	}

}
