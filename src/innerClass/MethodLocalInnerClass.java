package innerClass;

public class MethodLocalInnerClass 
{
	private static int x = 10;
	private int y = 20;
	
	/*
	 * If we make methodOuter or methodInner static we cannot access var x and y.
	 */
	public void methodOuter()
	{
		int z = 50;
		
		System.out.println("Local variable z for methodOuter :"+z);
		System.out.println("Inside Method Outer");
		class InnerClass
		{
			private int x = 100;			
			
			public void methodInner()
			{
				System.out.println("Inside Method Local Inner Class");
				/*
				 * Calling Instance variable of Inner Class
				 */
				System.out.println("Inner Class methodInner :"+this.x);
				System.out.println("Outer Class Variable x :"+MethodLocalInnerClass.x);
				System.out.println("Inner Class methodInner :"+y);
				System.out.println("***********************************************");
				System.out.println("Method Local variable z :"+z);
				
			}
		}
		
		InnerClass innerClass = new InnerClass();
		innerClass.methodInner();
		
	}
	public static void main(String[] args) 
	{		
		MethodLocalInnerClass outerClass = new MethodLocalInnerClass();
		outerClass.methodOuter();
	}
}
