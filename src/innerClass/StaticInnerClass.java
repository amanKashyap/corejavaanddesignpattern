package innerClass;

public class StaticInnerClass 
{
	static class NestedClass
	{
		/*public static void main(String[] args)
		{
			System.out.println("Hello from Nested Class Main");
		}*/
		
		public void nestedMethod()
		{
			System.out.println("Nested Class : nestedMethod");
		}
	}
	public static void main(String[] args) 
	{
		System.out.println("Hello from Outer Class");
		StaticInnerClass.NestedClass nested = new StaticInnerClass.NestedClass();
		nested.nestedMethod();
	}
}
