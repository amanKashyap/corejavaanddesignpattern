package innerClass;

public class AnonymousInnerClass 
{
	public void methodOuter()
	{
		System.out.println("Outer Class : methodOuter");
		TestClass aic = new TestClass()
						{
							@Override
							public void apply() 
							{
								System.out.println("Apply method called from Anonymous Inner Class");
								applyInner();
							}
							
							public void applyInner()
							{
								System.out.println("Inner method of AIC");
							}
						};
		aic.apply();
		/*
		 * We cannot Call additional created method of AIC from outside.
		 */
		//aic.applyInner();
	}
	
	public static void main(String[] args) 
	{	
		AnonymousInnerClass anonymousInnerClass = new AnonymousInnerClass();
		anonymousInnerClass.methodOuter();
		
		System.out.println("**************************************");
		System.out.println("Directly call apply() of TestClass");
		
		TestClass test = new TestClass();
		test.apply();
	}
}
class TestClass
{
	public void apply()
	{
		System.out.println("Apply called from Test Class");
	}
}