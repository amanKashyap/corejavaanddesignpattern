package basicConcepts;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8ShortCircuitMethod 
{
	public static void main(String[] args) 
	{
		List<String> names = Arrays.asList(new String[]{"barry","andy","bill","chris","Ben"});
		/*
		 * As limit() is a short circuit method so when Two names are obtained which start with B.
		 * It ends the operation and no other name is passed via map and filter operation.
		 */		
		List<String> nameStream =
		names.stream()
			 .map(name -> {
				 			System.out.println("Map Name :"+name);
				 			return name.toUpperCase();
			 			  })
			 .filter(name -> {
				 				System.out.println("Filter Name :"+name);
				 				return name.startsWith("B");				 				
			 				 })
			 .limit(2)
			 .collect(Collectors.toList());
			 

	}

}
