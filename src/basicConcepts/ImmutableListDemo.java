package basicConcepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImmutableListDemo 
{
	private List<String> list = new ArrayList<>();
	
	public ImmutableListDemo() 
	{
		list.add("A");
		list.add("B");
	}
	
	public ImmutableListDemo(List<String> finalList) 
	{
		list.clear();
		list.addAll(finalList);
	}

	public List<String> getList()
	{
		List<String> finalList = new ArrayList<>(list);
		return finalList;
	}
	
	public ImmutableListDemo getImmutableListDemo()
	{
		List<String> finalList = new ArrayList<>(list);
		ImmutableListDemo returnObj = new ImmutableListDemo(finalList);
		return returnObj;
	}
	
	public void addElement(String s)
	{
		final List<String> addList = new ArrayList<>();
		list.forEach(str -> addList.add(str));
		addList.add(s);
		list =  addList;
	}
	
	public List<String> getImmutableList() 
	{
	    List<String> finalList = new ArrayList<String>();
	    list.forEach(s -> finalList.add(s));
	    return finalList;
	}
	
	public void addOp(String str)
	{
		List<String> finalList = new ArrayList<String>();
		list.forEach(s -> finalList.add(s));
		finalList.add(str);		
	}
	
	
	public static void main(String[] args) 
	{
		ImmutableListDemo demo = new ImmutableListDemo();
		System.out.println("HashCode : "+demo.hashCode());
		System.out.println("Contents : "+ demo.getList()+" : "+demo.getList().hashCode());		
		demo.addElement("C");
		System.out.println("HashCode : "+demo.hashCode());
		System.out.println("Contents : "+ demo.getList()+" : "+demo.getList().hashCode());
		ImmutableListDemo demo1 = new ImmutableListDemo();
		System.out.println("HashCode : "+demo1.hashCode());
	}
	
	
	
	
	
	/*public static void main(String[] args) 
	{
		List<String> list = new ArrayList<String>(Arrays.asList("one", "two", "three"));
	    List<String> unmodifiableList = Collections.unmodifiableList(list);
	    System.out.println("Hash Code :"+unmodifiableList.hashCode());
	    unmodifiableList.add("four"); // java.lang.UnsupportedOperationException
	    
		ImmutableListDemo demo = new ImmutableListDemo();
		System.out.println("Hashcode :"+demo.getImmutableList().hashCode());
		System.out.println("Immutable List :"+demo.getImmutableList());
		demo.getImmutableList().add("C");
		System.out.println("Hashcode After Adding C:"+demo.getImmutableList().hashCode());
		System.out.println("Immutable List :"+demo.getImmutableList());
		
		ImmutableListDemo immutableListDemo = new ImmutableListDemo();
		System.out.println("ImmutableListDemo list :"+immutableListDemo.getImmutableList());
		System.out.println("ImmutableListDemo hashcode :"+immutableListDemo.hashCode());
		immutableListDemo.addOp("C");
		System.out.println("ImmutableListDemo list :"+immutableListDemo.getImmutableList());
		System.out.println("ImmutableListDemo hashcode :"+immutableListDemo.hashCode());
		
		
	}*/
}
