package basicConcepts;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashmapExample 
{
	public static void main(String[] args) 
	{
		Map<Integer, String> hashMap = new HashMap<>();
		
		for(int i =0 ; i<5 ; i++)
		{
			hashMap.put(i,"Test"+i);
		}
		System.out.println("HashMap :"+hashMap);
		
		Iterator<Integer> keySet = hashMap.keySet().iterator();
		
		while(keySet.hasNext())
		{
			Integer key = keySet.next();
			if(key%2 == 0)
			{
				hashMap.remove(key);
			}
			System.out.println("Key :"+ key);
			System.out.println("Value :"+hashMap.get(key));
		}
	}

}
