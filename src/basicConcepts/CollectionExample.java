package basicConcepts;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionExample
{
	public static void main(String[] args)
	{
		Integer[] a1 = new Integer[]{1,5,6,7,9};
		Integer[] a2 = new Integer[]{3,5,8,9,11};
		
		List<Integer> al = new ArrayList<>(Arrays.asList(a1));
		al.addAll(Arrays.asList(a2));
		
		System.out.println("Unsorted List: "+al);
		
		List<Integer> a3 = al.stream()
					   		 .sorted()
					   		 .collect(Collectors.toList());
		
		Integer[] sortedArray = new Integer[a3.size()];
		sortedArray = a3.toArray(sortedArray);
		System.out.println("Sorted Array :"+Arrays.toString(sortedArray));
		
		final List<Integer> finalArrayList = new ArrayList<>(a3);
		
		System.out.println("Final Array List :"+finalArrayList);
		
		finalArrayList.add(100);
		
		System.out.println("Updated Final Array List :"+finalArrayList);
		//finalArrayList = a3;
		
	}
}