package basicConcepts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordCountProblem 
{
	public static void main(String[] args) 
	{
		List<String> wordList = new ArrayList<String>();
		wordList.add("A");
		wordList.add("B");
		wordList.add("A");
		wordList.add("C");
		wordList.add("D");
		wordList.add("E");
		wordList.add("B");
		wordList.add("A");
		wordList.add("F");
		wordList.add("F");
		wordList.add("I");
		
		System.out.println(wordList);
		
		Map<String, Long> wordCount = wordList.stream()
												 .collect(Collectors.groupingBy(Function.identity(),
									 						Collectors.counting()));
				
		System.out.println(wordCount);

	}

}
