package basicConcepts;

import java.util.StringTokenizer;

/*
 * StringTokenizer class helps us split Strings into multiple tokens
 */
public class StringTokenizerDemo 
{
	public static void main(String[] args) 
	{
		String source = "This is a String-Tokenzier Demo";
		StringTokenizer myTokenizer = new StringTokenizer(source);
		
		System.out.println("Count of Token (myTokenizer) : "+myTokenizer.countTokens());
		
		while(myTokenizer.hasMoreTokens())
		{
			System.out.println("Token : "+myTokenizer.nextToken());
		}
		System.out.println("*****************************************************************");
		
		System.out.println("Count of Token (myTokenizer) : "+myTokenizer.countTokens());
		
		StringTokenizer newMyTokenizer = new StringTokenizer(source);
		System.out.println("Count of Token (newMyTokenizer): "+newMyTokenizer.countTokens());
		
		while(newMyTokenizer.hasMoreTokens())
		{
			System.out.println("Token : "+newMyTokenizer.nextToken("-"));
		}
		
		System.out.println("*****************************************************************");
		
		/*
		 * If returnDelims is made true then the delimiters would also be returned as tokens.
		 * By Default it is made false. 
		 * */
		StringTokenizer tokenizer = new StringTokenizer(source, " ", false);
		
		while(tokenizer.hasMoreTokens())
		{
			System.out.println("Tokens : "+tokenizer.nextToken());
		}
		
		System.out.println("*****************************************************************");
		System.out.println("Using String.split()");
		
		String[] splittedString = source.split(" ");
		for(String s : splittedString)
		{
			System.out.println(s);
		}
		
	}

}
