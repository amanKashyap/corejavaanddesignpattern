package chainOfResponsibilityDesignPattern;

public class Rupee10Dispencer implements ATMDispencer 
{
	private ATMDispencer dispencerChain;
	
	@Override
	public void setNextChain(ATMDispencer nextChain) 
	{
		dispencerChain = nextChain;
	}

	@Override
	public void dispense(Currency currency) 
	{
		if(currency.getAmount() >= 10)
		{
			int num = currency.getAmount()/10;
			int reminder = currency.getAmount() % 10;
			
			System.out.println("Dispence "+num+" Rupees 10 notes");
			
			if(reminder != 0)
			{
				this.dispencerChain.dispense(new Currency(reminder));
			}			
		}
		else
		{
			this.dispencerChain.dispense(currency);
		}		
	}
}
