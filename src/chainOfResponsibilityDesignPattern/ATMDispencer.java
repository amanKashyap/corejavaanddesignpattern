package chainOfResponsibilityDesignPattern;

public interface ATMDispencer 
{
	public void setNextChain(ATMDispencer nextChain);
	
	public void dispense(Currency currency);
	
}
