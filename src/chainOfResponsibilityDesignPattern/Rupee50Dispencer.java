package chainOfResponsibilityDesignPattern;

public class Rupee50Dispencer implements ATMDispencer 
{

	private ATMDispencer dispencerChain;
	
	@Override
	public void setNextChain(ATMDispencer nextChain) 
	{
		dispencerChain = nextChain;
	}

	@Override
	public void dispense(Currency currency) 
	{
		if(currency.getAmount() >= 50)
		{
			int num = currency.getAmount()/50;
			int reminder = currency.getAmount() % 50;
			
			System.out.println("Dispence "+num+" Rupees 50 notes");
			
			if(reminder != 0)
			{
				this.dispencerChain.dispense(new Currency(reminder));
			}			
		}
		else
		{
			this.dispencerChain.dispense(currency);
		}		
	}

}
