package chainOfResponsibilityDesignPattern;

import java.util.Scanner;

public class ATMDispencerChainTest 
{
	private ATMDispencer c1;

	public ATMDispencerChainTest() 
	{	
		
		this.c1 = new Rupee50Dispencer();
		ATMDispencer c2 = new Rupee20Dispencer();
		ATMDispencer c3 = new Rupee10Dispencer();

		// set the chain of responsibility
		c1.setNextChain(c2);
		c2.setNextChain(c3);
	}

	public static void main(String[] args) 
	{
		ATMDispencerChainTest atmDispenser = new ATMDispencerChainTest();
		while (true) {
			int amount = 0;
			System.out.println("Enter amount to dispense");
			Scanner input = new Scanner(System.in);
			amount = input.nextInt();
			if (amount % 10 != 0) {
				System.out.println("Amount should be in multiple of 10s.");
				return;
			}
			// process the request
			atmDispenser.c1.dispense(new Currency(amount));
		}

	}	
}
