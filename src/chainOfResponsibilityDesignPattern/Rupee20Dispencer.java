package chainOfResponsibilityDesignPattern;

public class Rupee20Dispencer implements ATMDispencer
{
private ATMDispencer dispencerChain;
	
	@Override
	public void setNextChain(ATMDispencer nextChain) 
	{
		dispencerChain = nextChain;
	}

	@Override
	public void dispense(Currency currency) 
	{
		if(currency.getAmount() >= 20)
		{
			int num = currency.getAmount()/20;
			int reminder = currency.getAmount() % 20;
			
			System.out.println("Dispence "+num+" Rupees 20 notes");
			
			if(reminder != 0)
			{
				this.dispencerChain.dispense(new Currency(reminder));
			}			
		}
		else
		{
			this.dispencerChain.dispense(currency);
		}		
	}
}
