package decoratorDesignPattern;

public class CarDecoratorPatternTest 
{
	public static void main(String[] args) 
	{
		Car basicCar = new BasicCar();
		basicCar.assemble();
		
		System.out.println("********************************");
		
		Car sportsCar = new SportsCar(new BasicCar());
		System.out.println("Assemble Car");
		sportsCar.assemble();
		
		System.out.println("********************************");
		
		Car luxurySportsCar = new LuxuryCar(new SportsCar(new BasicCar()));
		System.out.println("Assemble Car");
		luxurySportsCar.assemble();
	}

}
