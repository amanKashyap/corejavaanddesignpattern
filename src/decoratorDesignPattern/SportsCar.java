package decoratorDesignPattern;

public class SportsCar extends CarDecorator 
{
	public SportsCar(Car car) 
	{
		super(car);	
		System.out.println("Constructor : Sports Car");
	}
	
	@Override
	public void assemble() 
	{	
		super.assemble();
		System.out.println("Assemble : Sports Car");
	}
}
