package decoratorDesignPattern;

public class LuxuryCar extends CarDecorator 
{

	public LuxuryCar(Car car) 
	{
		super(car);	
		System.out.println("Constructor : Luxury Car");
	}
	
	@Override
	public void assemble() 
	{	
		super.assemble();
		System.out.println("Assemble : Luxury Car");
	}
}
