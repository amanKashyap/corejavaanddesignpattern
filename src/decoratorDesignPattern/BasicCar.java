package decoratorDesignPattern;

public class BasicCar implements Car 
{
	public BasicCar() 
	{
		System.out.println("Constructor : Basic Car");
	}
	@Override
	public void assemble() 
	{
		System.out.println("Assemble : Basic Car");
	}

}
