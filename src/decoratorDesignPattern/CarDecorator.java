package decoratorDesignPattern;

/*
 *  Decorator class implements the component interface and it has a HAS-A relationship with the component interface. 
 *  The component variable should be accessible to the child decorator classes, 
 *  so we will make this variable protected.
 *  
 *  Acting as mediator between Component Implementation i.e. Basic Car and Decorator Implementation i.e. Sports Car.
 */
public class CarDecorator implements Car 
{
	private Car car;
	
	public CarDecorator(Car car) 
	{
		this.car = car;
	}
	
	@Override
	public void assemble() 
	{
		this.car.assemble();
	}
}
