package multiThreading;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerProblem 
{
	public static void main(String[] args) 
	{	
		List<Integer> sharedQueue = new ArrayList<>();
		Producer producer = new Producer(sharedQueue);
		Consumer consumer = new Consumer(sharedQueue);
		
		Thread producerThread = new Thread(producer, "Producer Thread");
		Thread consumerThread = new Thread(consumer, "Consumer Thread");
		
		producerThread.start();
		consumerThread.start();
	}
}

class Producer implements Runnable 
{
	List<Integer> sharedQueue;
	
	//Max no of elements which can be produced simuntanously.
	private int maxSize = 2;
	
	public Producer(List sharedQueue) 
	{
		this.sharedQueue = sharedQueue;
	}

	@Override
	public void run() 
	{
		for(int i=1; i<10 ; i++)
		{
			
			try 
			{
				produce(i);
				Thread.sleep(1000);
			}
			catch (InterruptedException e) 
			{			
				e.printStackTrace();
			}
		}
	}
	
	private synchronized void produce(int i) throws InterruptedException
	{
		synchronized (sharedQueue) 
		{
			while(sharedQueue.size() == maxSize)
			{
				System.out.println("Queue is Full");
				System.out.println("Producer is waiting for Consumer to consume");
				System.out.println("SharedQueue size() :-"+sharedQueue.size());
				sharedQueue.wait();
			}
		}
		
		synchronized (sharedQueue) 
		{
			System.out.println("Produced : "+i);
			sharedQueue.add(i);
			Thread.sleep(1000);
			sharedQueue.notify();
		}
	}
}

class Consumer implements Runnable
{
	List<Integer> sharedQueue;
	
	public Consumer(List sharedQueue) 
	{
		this.sharedQueue = sharedQueue;
	}

	@Override
	public void run() 
	{
		while(true)
		{
			try
			{
				consume();
				Thread.sleep(1000);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
	}
	
	private synchronized void consume() throws InterruptedException
	{
		synchronized (sharedQueue) 
		{
			while(sharedQueue.size() == 0)
			{
				System.out.println("Queue is Empty!!!"+" Consumer is waiting for Producer to produce");
				Thread.sleep(500);
				sharedQueue.wait();
			}
		}
		
		synchronized (sharedQueue) 
		{
			System.out.println("Consumed : "+sharedQueue.remove(0));
			Thread.sleep(1000);
			sharedQueue.notify();
		}
	}
}
