package multiThreading;

public class LifecycleOfThreadExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
		Runnable myRunnable = () -> {
										System.out.println("Thread Name :"+Thread.currentThread().getName());
										for(int i=0; i<5; i++)
										{
											System.out.println("Print :"+i);
										}
									};
		Thread thread1 = new Thread();
		Thread thread2 = new Thread(myRunnable);
		
		//Start an New thread having empty run implentation.		
		thread1.start();
		
		//Calling run() as a normal Method executed by Main Thread.
		myRunnable.run();
		
		//Compile time error: Runnable doesn't contains run()
		/*myRunnable.start();*/
		
		//Start an New thread having run implentation given my myRunnable
		thread2.start();
		
		System.out.println("Sleep for 1 Sec");
		//Thread.sleep(1000);
		
		//Main Thread would complete move ahead once thread2 completes.
		thread2.join();
		System.out.println("End of Main");

	}

}
