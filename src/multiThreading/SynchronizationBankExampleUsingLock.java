package multiThreading;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizationBankExampleUsingLock 
{
	public static void main(String[] args) 
	{
		AccountLock account = new AccountLock(1,1000);
		BankLock bank1 = new BankLock(account,"withdraw",900);
		BankLock bank2 = new BankLock(account,"withdraw",600); 
		
		
		Thread thread1 = new Thread(bank1);
		Thread thread2 = new Thread(bank2);
		
		
		thread1.start();
		thread2.start();
	}

}
class AccountLock
{
	private Integer accountNo;
	private Integer balance;
	
	public AccountLock(Integer accountNo, Integer balance) 
	{		
		this.accountNo = accountNo;
		this.balance = balance;
	}
	
	public Integer getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(Integer accountNo) {
		this.accountNo = accountNo;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
}

class BankLock implements Runnable
{
	private AccountLock account;
	private String action;
	private Integer amount;
	private Lock l = new ReentrantLock();
	
	@Override
	public void run()
	{
		if(action.equalsIgnoreCase("withdraw"))
		{			
			withdraw(account, amount);		
		}
		else if(action.equalsIgnoreCase("deposit"))
		{
			deposit(account, amount);
		}
		else
		{
			System.out.println("Invalid Choice");
		}
	}
	
	public BankLock(AccountLock account,String action, Integer amount) 
	{
		this.account = account;
		this.action = action;
		this.amount = amount;
	}
	
	public void withdraw(AccountLock account, Integer amount)
	{
		Integer balance = account.getBalance();
		
		try
		{
		l.lock();	
		
		if(amount > balance)
			{
				System.out.println("Invalid Funds");
				System.out.println("Balance :"+account.getBalance());
				
			}
			else
			{
				balance = balance - amount;
				account.setBalance(balance);
				System.out.println("Transaction Sucessful");
				System.out.println("Balance :"+account.getBalance());
			}
		
		}
		finally 
		{
			l.unlock();
		}
	}		
	
	
	public synchronized void deposit(AccountLock account, Integer amount)
	{
		Integer balance = account.getBalance();
		
		try
		{
		l.lock();
		if(amount > 10000)
			{
				System.out.println("Deposit Limit Exceedded");
				
			}
			else
			{
				balance = balance + amount;
				account.setBalance(balance);
				System.out.println("Transaction Sucessful");
				System.out.println("Balance :"+account.getBalance());
			}
		}
		finally 
		{
			l.unlock();
		}
	}
}