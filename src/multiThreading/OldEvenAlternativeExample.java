package multiThreading;

import java.util.ArrayList;
import java.util.List;

/*
 * Threads.Program : Print 1, 2 , 3 , 4 ,5 , 6 odd even alternate using threads. 
 * Tell me what are the classes you may write: 
 */
public class OldEvenAlternativeExample 
{
	public static void main(String[] args) 
	{
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		
		System.out.println("List of Numbers :"+numbers);
		OddThread odd = new OddThread(numbers);
		EvenThread even = new EvenThread(numbers);
		
		odd.start();
		even.start();

	}

}
class OddThread extends Thread
{
	private List<Integer> numbers;
	
	public OddThread(List<Integer> numbers)	
	{	
		this.numbers = numbers;
	}
	
	@Override
	public void run()
	{
		synchronized (numbers) 
		{				
			for(Integer i :numbers)
			{
				if(i%2 != 0)
				{
					System.out.println(i);
					try {
						numbers.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					numbers.notify();
				}
			}
		}
	}
}

class EvenThread extends Thread
{
private List<Integer> numbers;
	
	public EvenThread(List<Integer> numbers)	
	{	
		this.numbers = numbers;
	}
	
	@Override
	public void run()
	{
		synchronized (numbers) 
		{				
			for(Integer i :numbers)
			{
				if(i%2 == 0)
				{
					System.out.println(i);
					try {
						numbers.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					numbers.notify();
				}
			}
		}
	}

}