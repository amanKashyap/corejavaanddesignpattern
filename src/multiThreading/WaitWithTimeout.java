package multiThreading;

public class WaitWithTimeout {
	static Object lock = new Object();
	public static void main(String[] args) {
		Runnable myRunnable1 = new Runnable() {
			public void run() {
				synchronized (lock) {
					try {
						System.out.println(Thread.currentThread().getName() + " entered");
						lock.wait();
						System.out.println("Wait completed");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName() + " exited");
				}
			}
		};
		Runnable myRunnable2 = new Runnable() {
			public void run() {
				System.out.println("myRunnable 2 entered");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				synchronized (lock) {
					try {
						System.out.println(Thread.currentThread().getName() + " entered");
						Thread.sleep(5000); // wait() internally calls wait(0)
						System.out.println("Sleep completed");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		Thread thread1 = new Thread(myRunnable1, "Thread-1");
		Thread thread2 = new Thread(myRunnable2, "Thread-2");
		thread1.start();
		thread2.start();
	}
}
