package multiThreading;

public class BusRouteProblemUsingWait implements Runnable 
{

	static int numThread = 1;
    static int threadAllowedToRun = 1;
    int myThreadID;
    private static Object myLock = new Object();

    public BusRouteProblemUsingWait() 
    {
        this.myThreadID = numThread++;
        System.out.println("Bus ID:" + myThreadID);
    }

    @Override
    public void run() {
        synchronized (myLock) 
        {
            while (myThreadID != threadAllowedToRun) 
            {
                try 
                {
                    myLock.wait();
                } 
                catch (InterruptedException e) 
                {

                } 
                catch (Exception e) {}
            }
            try 
            {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {}

            System.out.println("Reached Destination Bus :" + myThreadID);
            myLock.notifyAll();
            threadAllowedToRun++;
        }
    }
	
	public static void main(String[] args) 
	{    

        Thread t1 = new Thread(new BusRouteProblemUsingWait());
        Thread t2 = new Thread(new BusRouteProblemUsingWait());
        Thread t3 = new Thread(new BusRouteProblemUsingWait());
        Thread t4 = new Thread(new BusRouteProblemUsingWait());
        Thread t5 = new Thread(new BusRouteProblemUsingWait());

        t5.start();
        t4.start();
        t3.start();
        t2.start();
        t1.start();

    }

}
