package multiThreading;



public class AdditionViaMultiThreading 
{
	public static void main(String[] args) 
	{
		Thread thread1 = new Thread(new Calculate());
		Thread thread2 = new Thread(new Calculate());
		Thread thread3 = new Thread(new Calculate());
		Thread thread4 = new Thread(new Calculate());
		
		/*Different instances of Calculate used.
		  Hence, Class level Lock used */
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();

	}

}

class Calculate implements Runnable
{
	int counter = 0;
	
	@Override
	public void run()
	{
		//Need Synchronization		
		synchronized (Calculate.class) 
		{
			add();
			sub();
		}		
		//Only read Operation : No synchronization required
		read();
	}
	
	public void add()
	{
		System.out.println("Increment Counter :"+counter++);
	}
	public void sub()
	{
		System.out.println("Decrement Counter :"+counter--);
	}
	public void read()
	{
		System.out.println("Counter :"+counter);
	}
}