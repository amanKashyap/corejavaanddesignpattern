package multiThreading;

public class BusRouteProblem 
{
	public static void main(String[] args) 
	{
		Runnable bus = () -> System.out.println(Thread.currentThread().getName()+" Has reached point B");
		
		Thread bus1 = new Thread(bus, "Bus1");
		Thread bus2 = new Thread(bus, "Bus2");
		Thread bus3 = new Thread(bus, "Bus3");
		Thread bus4 = new Thread(bus, "Bus4");
		Thread bus5 = new Thread(bus, "Bus5");
		
		try
		{
		bus1.start();
		bus1.join();
		
		bus2.start();
		bus2.join();
		
		bus3.start();
		bus3.join();
		
		bus4.start();
		bus4.join();
		
		bus5.start();
		bus5.join();
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
	}
}
