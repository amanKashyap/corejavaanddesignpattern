package multiThreading;


public class SynchronizationBankExample 
{
	public static void main(String[] args) 
	{
		Account account = new Account(1,1000);
		Bank bank1 = new Bank(account,"withdraw",900);
		Bank bank2 = new Bank(account,"withdraw",600); 
		
		
		Thread thread1 = new Thread(bank1);
		Thread thread2 = new Thread(bank2);
		
		
		thread1.start();
		thread2.start();
	}

}

class Account
{
	private Integer accountNo;
	private Integer balance;
	
	public Account(Integer accountNo, Integer balance) 
	{		
		this.accountNo = accountNo;
		this.balance = balance;
	}
	
	public Integer getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(Integer accountNo) {
		this.accountNo = accountNo;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
}

class Bank implements Runnable
{
	private Account account;
	private String action;
	private Integer amount;
	
	@Override
	public void run()
	{
		if(action.equalsIgnoreCase("withdraw"))
		{
			withdraw(account, amount);
		}
		else if(action.equalsIgnoreCase("deposit"))
		{
			deposit(account, amount);
		}
		else
		{
			System.out.println("Invalid Choice");
		}
	}
	
	public Bank(Account account,String action, Integer amount) 
	{
		this.account = account;
		this.action = action;
		this.amount = amount;
	}
	
	public void withdraw(Account account, Integer amount)
	{
		Integer balance = account.getBalance();
		synchronized (account) 
		{
			if(amount > balance)
			{
				System.out.println("Invalid Funds");
				System.out.println("Balance :"+account.getBalance());
				
			}
			else
			{
				balance = balance - amount;
				account.setBalance(balance);
				System.out.println("Transaction Sucessful");
				System.out.println("Balance :"+account.getBalance());
			}
		}
		
	}
	
	public synchronized void deposit(Account account, Integer amount)
	{
		Integer balance = account.getBalance();
		synchronized (account) 
		{
			if(amount > 10000)
			{
				System.out.println("Deposit Limit Exceedded");
				
			}
			else
			{
				balance = balance + amount;
				account.setBalance(balance);
				System.out.println("Transaction Sucessful");
				System.out.println("Balance :"+account.getBalance());
			}
		}
		
	}
}


