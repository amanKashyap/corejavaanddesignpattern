package multiThreading;

import java.io.FileWriter;

public class CurrentThreadProblem 
{
	public static void main(String[] args) 
	{
		Runnable myRunnable = () -> {
										if(Thread.currentThread().getName().equals("Thread1"))
										{
											for(int i =1; i<= 10; i++)
											{
												System.out.println(i);
											}
										}
										else if(Thread.currentThread().getName().equals("Thread2"))
										{
											for(int i =11; i<= 20; i++)
											{
												System.out.println(i);
											}
										}
												
									};
									
		Thread thread1 = new Thread(myRunnable,"Thread1");
		Thread thread2 = new Thread(myRunnable,"Thread2");
		
		thread1.start();
		thread2.start();

	}

}

/*class myRunable implements Runnable
{

	@Override
	public void run() 
	{
		FileWriter out = new FileWriter("out.txt");
		out.append('a');
		
	}
	
}*/