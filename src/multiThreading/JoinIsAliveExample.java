package multiThreading;

public class JoinIsAliveExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
		Runnable r = () -> {
								try 
								{
									Thread.sleep(500);
								}
								catch (InterruptedException e) 
								{								
									e.printStackTrace();
								}
								for(int i = 0; i<5 ; i++)
								{
									System.out.println("Counter "+i);
								}
						   };
		myRunnable runnable = new myRunnable(Thread.currentThread());
		Thread rThread = new Thread(r);
		Thread myRunnableThread = new Thread(runnable);
		
		rThread.start();
		myRunnableThread.start();
		
		System.out.println("Waiting for rThread to complete");
		System.out.println("Calling join() on rThread");
		rThread.join();
		
		System.out.println("rThread Dead :"+ !rThread.isAlive());
		System.out.println("End of Main");
	}

}

class myRunnable implements Runnable
{
	static private Thread mainThread;
	
	public myRunnable(Thread mainThread)
	{
		this.mainThread = mainThread;
	}
	
	@Override
	public void run() 
	{
		System.out.println("Waiting for Main Thread to complete");
		System.out.println("Calling join() on Main Thread");
		try 
		{
			mainThread.join();
		} 
		catch (InterruptedException e) 
		{		
			e.printStackTrace();
		}
		
		System.out.println("Main Thread Dead :"+ !mainThread.isAlive());
		System.out.println("Wait Completed: Ending run() of myRunnable");				
	}
	
}