package multiThreading;

import java.lang.Thread.UncaughtExceptionHandler;
import java.security.cert.Extension;
import java.util.Collection;

public class UncaughtExceptionHandlerDemo 
{
	public static void main(String[] args) 
	{
		Task task = new Task();
		Thread thread1 = new Thread(task,"Thread 1");
		thread1.start();
	}
}

class ExceptionHandler implements UncaughtExceptionHandler
{

	@Override
	public void uncaughtException(Thread t, Throwable e) 
	{
		System.out.println("Exception Caught in :"+t.getName());
		System.out.println("Excaption Captured");
		e.printStackTrace();				
	}	
}

class Task implements Runnable
{
	@Override
	public void run() 
	{
		  Thread.currentThread().setUncaughtExceptionHandler(new ExceptionHandler());
	      System.out.println(Integer.parseInt("123"));
	      System.out.println(Integer.parseInt("234"));
	      System.out.println(Integer.parseInt("345"));
	      System.out.println(Integer.parseInt("XYZ")); //This will cause NumberFormatException
	      System.out.println(Integer.parseInt("456"));		
	}	
}

