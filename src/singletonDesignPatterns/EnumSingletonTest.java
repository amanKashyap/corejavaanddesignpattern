package singletonDesignPatterns;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class EnumSingletonTest 
{
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
	{
		EnumSingleton enum1 = EnumSingleton.ENUM_SINGLETON_INSTANCE;
		System.out.println("enum1 hashcode() :"+enum1.hashCode());
		
		EnumSingleton enum2 = EnumSingleton.ENUM_SINGLETON_INSTANCE;
		System.out.println("enum2 hashcode() :"+enum2.hashCode());
		
		/*
		 * No Constuctor is present in Enum.
		 * Below code is proving the same
		 */
		Constructor<?>[] constructors = EnumSingleton.class.getConstructors();
		for(Constructor<?> constructor : constructors)
		{
			constructor.setAccessible(true);
			EnumSingleton enum3 = (EnumSingleton) constructor.newInstance();
			System.out.println("enum3 hashcode() :"+enum3.hashCode());
		}
		
		Runnable myRunnable = () -> {
										EnumSingleton enumThread = EnumSingleton.ENUM_SINGLETON_INSTANCE;
										System.out.println(Thread.currentThread().getName()+" enumThread hashcode() :"+enumThread.hashCode());
									};
		System.out.println("Using multiple threads");
		Thread tread1 = new Thread(myRunnable,"Thread1");
		tread1.start();
		Thread tread2 = new Thread(myRunnable,"Thread2");
		tread2.start();
	}
}

enum EnumSingleton
{
	ENUM_SINGLETON_INSTANCE;
}