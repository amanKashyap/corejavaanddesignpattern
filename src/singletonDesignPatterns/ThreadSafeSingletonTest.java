package singletonDesignPatterns;


public class ThreadSafeSingletonTest 
{	
	public static void main(String[] args) throws InterruptedException 
	{
		System.out.println("****************Using Synchronized Method*************************");
		ThreadSafeSingleton t1 = ThreadSafeSingleton.getInstance();
		System.out.println(Thread.currentThread().getName()+" hashcode() :"+t1.hashCode());
		
		Runnable r = () -> {
			ThreadSafeSingleton t2 = ThreadSafeSingleton.getInstance();
			System.out.println(Thread.currentThread().getName()+" hashcode() :"+t2.hashCode());
	   };	
	   
	   Thread thread1 = new Thread(r,"Thread1");
	   thread1.start();
	   
	   Thread thread2 = new Thread(r,"Thread2");
	   thread2.start();
	   
	   Thread.sleep(500);
	   System.out.println("****************Using Double Checking*************************");
	   
	   Runnable myRunnable = () -> {
			ThreadSafeSingleton t2 = ThreadSafeSingleton.getDoubleCheckingInstance();
			System.out.println(Thread.currentThread().getName()+" hashcode() :"+t2.hashCode());
	   };
	   Thread thread3 = new Thread(myRunnable,"Thread3");
	   thread3.start();
	   
	   Thread thread4 = new Thread(myRunnable,"Thread4");
	   thread4.start();
	   
	}
}

/*
 * Eager, Static Block and Lazy Initialization would fail in
 * case of multiThreading.
 * 
 * To ensure our Singleton pattern holds true for multiThreaded enviroment.
 * we can use following ways:
 * 1> Synchronized getInstance method
 * 2> synchronized block with double checking
 */
class ThreadSafeSingleton
{
	private static ThreadSafeSingleton TS_SINGLETON;
	
	//Private Constructor
	private ThreadSafeSingleton(){}
	
	/*
	 * Synchronized method would create performance issue.
	 * As we required synchronization just for first few thread which would actually require to create the Object.
	 */
	public static synchronized ThreadSafeSingleton getInstance()
	{
		if(TS_SINGLETON == null)
		{
			TS_SINGLETON = new ThreadSafeSingleton();
		}
		return TS_SINGLETON;
	}
	
	/*
	 * lazy initialization why needed? 
	 * Ans: Perhaps creating a Resource is an expensive operation, 
	 * and users of SomeClass might not actually call getResource() in any given run.
	 */
	/*
	 * DCL(Double Checking Lock) also averts the race condition by checking resource a second time 
	   inside the synchronized block; 
	 * That ensures that only one thread will try to initialize resource.
	 * Suppose thread B comes along after the memory has been allocated and the resource field is set, 
	   but before the constructor is called. It sees that resource is not null, 
	   skips the synchronized block, and returns a reference to a partially constructed Resource! 
	 * Needless to say, the result is neither expected nor desired.
	 * 
	 * Soltuion can be to make all the member variables as volatile.
	 * https://www.javaworld.com/article/2074979/java-concurrency/double-checked-locking--clever--but-broken.html
	 */
	public static ThreadSafeSingleton getDoubleCheckingInstance()
	{
		if(TS_SINGLETON == null)
		{
			synchronized (ThreadSafeSingleton.class) 
			{
				if(TS_SINGLETON == null)
				{
					TS_SINGLETON = new ThreadSafeSingleton();
				}
			}
		}
		return TS_SINGLETON;
	}

}