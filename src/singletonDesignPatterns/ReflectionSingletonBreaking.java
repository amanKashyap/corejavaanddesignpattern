package singletonDesignPatterns;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
 * Singleton pattern as defined in previous classes can be broken by using:
 * 1>Reflection
 * 2>Serialization
 * 3>Cloneable
 */
public class ReflectionSingletonBreaking 
{
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException, ClassNotFoundException, CloneNotSupportedException 
	{
		System.out.println("*************Breaking the Singleton Pattern Using Reflection***************");
		/*
		 * Create Enum Singleton to prevent breaking Singleton pattern from Reflection.
		 */
		ThreadSafeSingletonUsingDoubleCheckLock obj1 = ThreadSafeSingletonUsingDoubleCheckLock.getDoubleCheckingInstance();
		ThreadSafeSingletonUsingDoubleCheckLock obj2 = null;
		System.out.println("Obj1 hashcode() :"+obj1.hashCode());
		
		Constructor<?>[] constructors = ThreadSafeSingletonUsingDoubleCheckLock.class.getDeclaredConstructors();
		for(Constructor<?> constructor : constructors)
		{
			constructor.setAccessible(true);
			obj2 = (ThreadSafeSingletonUsingDoubleCheckLock) constructor.newInstance();
			break;
		}
		System.out.println("Obj2 hashcode() :"+obj2.hashCode());
		
		
		System.out.println("*************Breaking the Singleton Pattern Using Serialization***************");
		/*
		 * Override readResolve() to prevent breaking Singleton pattern from Serialization.
		 */
		FileOutputStream fos = new FileOutputStream("wxyz.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		System.out.println("Writing Obj1 into stream having hashcode() :"+obj1.hashCode());
		oos.writeObject(obj1);
		
		FileInputStream fis = new FileInputStream("wxyz.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		ThreadSafeSingletonUsingDoubleCheckLock obj3 = (ThreadSafeSingletonUsingDoubleCheckLock) ois.readObject();
		System.out.println("Reading Obj3 from stream hashcode() :"+obj3.hashCode());
		
		System.out.println("*************Breaking the Singleton Pattern Using Cloneable***************");
		/*
		 * Override clone() to throw CloneNotSupportedException to prevent breaking Singleton pattern from Cloneable.
		 */
		System.out.println("Obj1 hashcode() :"+obj1.hashCode());
		System.out.println("Cloning Obj 1");
		ThreadSafeSingletonUsingDoubleCheckLock obj4 = (ThreadSafeSingletonUsingDoubleCheckLock) obj1.clone();
		System.out.println("Obj4 after Cloning hashcode() :"+obj4.hashCode());
	}
}
class ThreadSafeSingletonUsingDoubleCheckLock implements Serializable,Cloneable
{
	private static ThreadSafeSingletonUsingDoubleCheckLock TS_SINGLETON;
	
	private ThreadSafeSingletonUsingDoubleCheckLock(){}

	public static ThreadSafeSingletonUsingDoubleCheckLock getDoubleCheckingInstance()
	{
		if(TS_SINGLETON == null)
		{
			synchronized (ThreadSafeSingletonUsingDoubleCheckLock.class) 
			{
				if(TS_SINGLETON == null)
				{
					TS_SINGLETON = new ThreadSafeSingletonUsingDoubleCheckLock();
				}
			}
		}
		return TS_SINGLETON;
	}
	
	/*
	 * Uncomment below line to prevent Serialization of Singleton Class.
	 */
	/*protected Object readResolve()
    {
        return TS_SINGLETON;
    }*/
	
	@Override
	public Object clone() throws CloneNotSupportedException 
	{	
		return super.clone();
		
		/*
		 * Uncomment below line to prevent Cloning of Singleton Class.
		 */
		//throw new CloneNotSupportedException("Cannot Clone Singleton Class");
	}

}