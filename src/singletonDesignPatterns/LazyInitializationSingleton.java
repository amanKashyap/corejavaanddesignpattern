package singletonDesignPatterns;

public class LazyInitializationSingleton 
{
	/*
	 * Object creation is done lazily.
	 * Whenever we call getInstance() which would check is the instance is null or not.
	 * if null then only Object creation will take place.
	 */
	private static LazyInitializationSingleton LA_INITIALIZATION_SINGLETON;
	
	private LazyInitializationSingleton(){}
	
	public static LazyInitializationSingleton getInstance()
	{
		if(LA_INITIALIZATION_SINGLETON == null)
		{
			LA_INITIALIZATION_SINGLETON = new LazyInitializationSingleton();
		}
		return LA_INITIALIZATION_SINGLETON;
	}
	
	public static void main(String[] args) 
	{
		LazyInitializationSingleton l1 = LazyInitializationSingleton.getInstance();
		System.out.println("l1 hashcode() : "+l1.hashCode());
		
		LazyInitializationSingleton l2 = LazyInitializationSingleton.getInstance();
		System.out.println("l1 hashcode() : "+l2.hashCode());

	}

}
