package singletonDesignPatterns;

public class BillPughSingletonTest 
{
	public static void main(String[] args) 
	{
		BillPughSingleton b1 = BillPughSingleton.getInstance();
		System.out.println(Thread.currentThread().getName()+" hashcode() :"+b1.hashCode());
		
		Runnable r = () -> {
			BillPughSingleton b2 = BillPughSingleton.getInstance();
			System.out.println(Thread.currentThread().getName()+" hashcode() :"+b2.hashCode());
	   };	
	   
	   Thread thread1 = new Thread(r,"Thread1");
	   thread1.start();
	   
	   Thread thread2 = new Thread(r,"Thread2");
	   thread2.start();

	}
}

/*
 * Uses Inner static helper class.
 * When the singleton class is loaded, SingletonHelper class is not loaded into memory and 
 * only when someone calls the getInstance method, 
 * this class gets loaded and creates the Singleton class instance.
 * 
 * safety provided while using multiple threads.
 */
class BillPughSingleton
{
	private BillPughSingleton BP_SINGLETON;
	
	private BillPughSingleton() {}
	
	//Private Static inner class
	private static class BillPughSingletonHelper
	{
		private static final BillPughSingleton BP_SINGLETON_INSTANCE = new BillPughSingleton();
	}
	
	public static BillPughSingleton getInstance()
	{
		return BillPughSingletonHelper.BP_SINGLETON_INSTANCE;
	}
}