package singletonDesignPatterns;

public class StaticBlockEagerInitializationSingleton 
{
	/*
	 * STATIC_BLOCK_INITIALIZATION_SINGLETON not taken as final because object creation is surrounded by try-catch block.
	 * 
	 * Difference between Eager Initialization and Static Block initialization is that we can surround object creating in 
	 * try-catch block in static block initialization method.
	 */
	private static StaticBlockEagerInitializationSingleton STATIC_BLOCK_INITIALIZATION_SINGLETON;
	
	static
	{
		try
		{
		STATIC_BLOCK_INITIALIZATION_SINGLETON = new StaticBlockEagerInitializationSingleton();
		}
		catch(Exception e)
		{
			System.out.println("Exception Occured in Singleton Instance creation");
		}
	}
	
	private StaticBlockEagerInitializationSingleton(){}
	
	public static StaticBlockEagerInitializationSingleton getInstance()
	{
		return STATIC_BLOCK_INITIALIZATION_SINGLETON;
	}
	
	public static void main(String[] args) 
	{
		StaticBlockEagerInitializationSingleton s1 = StaticBlockEagerInitializationSingleton.getInstance();
		System.out.println("s1 hashcode() : "+s1.hashCode());
		
		StaticBlockEagerInitializationSingleton s2 = StaticBlockEagerInitializationSingleton.getInstance();
		System.out.println("s2 hashcode() : "+s2.hashCode());
	}

}
