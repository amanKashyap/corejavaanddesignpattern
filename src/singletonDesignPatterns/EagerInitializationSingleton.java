package singletonDesignPatterns;

public class EagerInitializationSingleton 
{
	/*
	 * private EagerInitializationSingleton object which is initialized at the time of Class loading only.
	 */
	private static final EagerInitializationSingleton EA_INITIALIZATION_SINGLETON = new EagerInitializationSingleton();
	
	private EagerInitializationSingleton(){}
	
	public static EagerInitializationSingleton getInstance()
	{
		return EA_INITIALIZATION_SINGLETON;
	}
	
	public static void main(String[] args) 
	{		
		EagerInitializationSingleton e1 = EagerInitializationSingleton.getInstance();
		System.out.println("E1 hashcode() :"+e1.hashCode());
		
		EagerInitializationSingleton e2 = EagerInitializationSingleton.getInstance();
		System.out.println("E2 hashcode() :"+e2.hashCode());
				
	}
}
