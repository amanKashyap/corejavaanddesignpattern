package builderDesignPattern;

public class Computer 
{
	
	//required parameters
	private String HDD;
	private String RAM;
	
	//optional parameters
	private boolean isAdditionalGraphicsCardEnabled;
	private boolean isBluetoothEnabled;
	

	public String getHDD() 
	{
		return HDD;
	}

	public String getRAM() 
	{
		return RAM;
	}

	public boolean isAdditionalGraphicsCardEnabled() 
	{
		return isAdditionalGraphicsCardEnabled;
	}

	public boolean isBluetoothEnabled() 
	{
		return isBluetoothEnabled;
	}
	
	private Computer(ComputerBuilder builder) 
	{
		this.HDD=builder.HDD;
		this.RAM=builder.RAM;
		this.isAdditionalGraphicsCardEnabled=builder.isAdditionalGraphicsCardEnabled;
		this.isBluetoothEnabled=builder.isBluetoothEnabled;
	}
	
	//Builder Class -- static inner class
	static class ComputerBuilder
	{

		// required parameters
		private String HDD;
		private String RAM;

		// optional parameters
		private boolean isAdditionalGraphicsCardEnabled;
		private boolean isBluetoothEnabled;
		
		public ComputerBuilder(String hdd, String ram)
		{
			this.HDD=hdd;
			this.RAM=ram;
		}

		public ComputerBuilder setAdditionalGraphicsCardEnabled(boolean isAdditionalGraphicsCardEnabled) 
		{
			this.isAdditionalGraphicsCardEnabled = isAdditionalGraphicsCardEnabled;
			return this;
		}

		public ComputerBuilder setBluetoothEnabled(boolean isBluetoothEnabled) 
		{
			this.isBluetoothEnabled = isBluetoothEnabled;
			return this;
		}
		
		public Computer build()
		{
			return new Computer(this);
		}
	}


}

