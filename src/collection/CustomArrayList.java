package collection;


import java.util.Arrays;


public class CustomArrayList<E>
{
	
	private E[] elementArray;
	private static int INITIAL_SIZE = 15;
	private static Object[] DEFAULT_EMPTY_ARRAY = {};
	private int size;
	private int currentCapacity;
	private int newCapacity;
	
	public static void main(String[] args)
	{
		CustomArrayList<Integer> customArrayList = new CustomArrayList<>();
		customArrayList.add(10);
		customArrayList.add(15);
		customArrayList.add(13);
		
		System.out.println("Size : "+customArrayList.size());
		System.out.println("Contains :"+customArrayList.contains(10));
		System.out.println("GET index 2 :"+customArrayList.get(2));
		System.out.println("GET index 10 :"+customArrayList.get(10));
	}
	
	public CustomArrayList()
	{
		elementArray = (E[]) DEFAULT_EMPTY_ARRAY;
		size =0;
	}
	
	public int size() 
	{
		return size;
	}
	
	public boolean isEmpty() 
	{	
		return size == 0;
	}

	public boolean contains(Object o) 
	{	
		if(o != null)
		{
			for(int i =0 ; i<size ; i++)
			{
				if(elementArray[i].equals(o))
				{
					return true;
				}
			}
		}
		else
		{
			for(int i =0 ; i<size ; i++)
			{
				if(elementArray[i] == null)
				{
					return true;
				}
			}
		}
		return false;
	}

	public boolean add(E e) 
	{
		if(elementArray == DEFAULT_EMPTY_ARRAY)
		{
			elementArray = Arrays.copyOf(elementArray,INITIAL_SIZE);
			currentCapacity = INITIAL_SIZE;
		}
		else if(size == currentCapacity)
		{
			newCapacity = currentCapacity*2;
			elementArray = Arrays.copyOf(elementArray,newCapacity);			
		}
		elementArray[size++] = e;
		
		return true;
	}

	public E get(int index) 
	{
		indexRangeValidation(index);		
		return elementArray[index];
	}

	private void indexRangeValidation(int index)
	{
		if(index >= size)
		{
			throw new IndexOutOfBoundsException("Invalid Index Passed");
		}
	}

}
