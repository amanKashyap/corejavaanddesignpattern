package collection;

import java.util.ArrayList;
import java.util.Spliterator;

public class SplitIteratorDemo 
{
	public static void main(String[] args) 
	{
		ArrayList<Integer> arrayList = new ArrayList<>();
		for(int i = 1; i< 11 ; i++)
		{
			arrayList.add(i);					
		}
		System.out.println("Elements added to ArrayList :");
		System.out.println(arrayList);
		
		System.out.println("Creating SplitIterator from the arrayList");
		Spliterator<Integer> splitIterator = arrayList.spliterator();
		
		System.out.println("Estimate Size :"+splitIterator.estimateSize());
		System.out.println("Printing all elements of splitIterator:");
		splitIterator.forEachRemaining(System.out :: println);
		
		/*Spliterator<Integer> trySplit = splitIterator.trySplit();
		System.out.println("Printing elements of TrySplit");
		trySplit.forEachRemaining(System.out :: println);*/
		
		System.out.println("Estimate Size :"+splitIterator.estimateSize());
		
		
	}

}
