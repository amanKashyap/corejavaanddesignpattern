package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SecondHighestNumberFromArray 
{
	public static void main(String[] args) 
	{
		Integer[] inputArray = new Integer[]{1,5,2,4,8,7,15};
		
		//Approach 1:
		int max = inputArray[0];
		int secondMax = 0;
		
		for(Integer i :inputArray)
		{
			if(max<=i)
			{
				secondMax = max;
				max = i;				
			}
		}
		System.out.println("Second Highest Number is :"+secondMax);
		
		//Approach 2:
		List<Integer> arrayList2 = new ArrayList<>(Arrays.asList(inputArray));

		arrayList2 = Stream.of(inputArray)
						   .sorted()
						   .limit(inputArray.length-1)
						   .collect(Collectors.toList());
		
		System.out.println("Second Highest is :"+arrayList2.get(arrayList2.size()-1));		
		
		

	}

}
