package collection;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sound.midi.ControllerEventListener;

public class MergeList 
{
	public static void main(String[] args) 
	{
		//Approach 1
		List<Integer> linkedList1 = new LinkedList<>();
		linkedList1.add(0);
		linkedList1.add(1);
		linkedList1.add(3);
		linkedList1.add(4);
		linkedList1.add(7);
		linkedList1.add(8);
		
		List<Integer> linkedList2 = new LinkedList<>();
		linkedList2.add(1);
		linkedList2.add(2);
		linkedList2.add(5);
		linkedList2.add(6);
		linkedList2.add(9);
		
		List<Integer> linkedList3 = new LinkedList<>();
		linkedList3.addAll(linkedList1);
		linkedList3.addAll(linkedList2);
		
		System.out.println("Unsorted Linked List 3 :"+linkedList3);
		linkedList3 = linkedList3.stream()
								 .sorted()
								 .collect(Collectors.toList());
		
		System.out.println("Sorted Linked List 3 using Approach 1: "+linkedList3);
				   
		//Approach 2:
		linkedList3 = Stream.of(linkedList1.stream(),linkedList2.stream())
						    .flatMap(x -> x)
							.sorted()
						    .collect(Collectors.toList());
		System.out.println("Sorted Linked List using Approach 2 :"+linkedList3);
		
		//Approach 3:
		List<Integer> allList = new LinkedList<>();
		int linkedList1Size = linkedList1.size();
		int linkedList2Size = linkedList2.size();
		int i = 0;
		int j = 0;
		
		while(i<linkedList1Size && j< linkedList1Size)
		{
			if(linkedList1.get(i)<= linkedList2.get(j))
			{
				allList.add(linkedList1.get(i));
				i++;
			}
			else if(linkedList1.get(i) > linkedList2.get(j))
			{
				allList.add(linkedList2.get(j));
				j++;
			}
		}
		if(i != linkedList1Size)
		{
			while(i<linkedList1Size)
			{
				allList.add(linkedList1.get(i));
				i++;
			}
		}
		else if(j != linkedList2Size)
		{
			while (j<linkedList2Size) 
			{
				allList.add(linkedList2.get(j));
				j++;
			}
		}
						
		System.out.println("All List : "+allList);
		

	}

}
