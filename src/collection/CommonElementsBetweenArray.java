package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CommonElementsBetweenArray 
{
	public static void main(String[] args) 
	{
		//Approach One: Using Contains
		Integer[] array1 = new Integer[]{1,2,3,4,5,5};
		Integer[] array2 = new Integer[]{4,5,6,7,8};
		
		List<Integer> arrayList1 = new ArrayList<Integer>(Arrays.asList(array1));
		
		System.out.println("Common Elements Are :" );
		for(int i : array2)
		{
			
			if(arrayList1.contains(i))
			{
				System.out.println(i);
			}
		}
		
		//Approach Two: Using RetainAll
		List<Integer> arrayList2 = new ArrayList<>(Arrays.asList(array2));
		arrayList1.retainAll(arrayList2);
		
		//Duplicates Present
		System.out.println("Common Elements Are : "+arrayList1);
		
		Set<Integer> set1 = new HashSet<>(Arrays.asList(array1));
		Set<Integer> set2 = new HashSet<>(Arrays.asList(array2));
		
		set1.retainAll(set2);
		//No Duplicates
		System.out.println("Common Elements Are : "+set1);
	}
}
