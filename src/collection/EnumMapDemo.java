package collection;

import java.util.EnumMap;
import java.util.Iterator;

public class EnumMapDemo 
{
	public enum STATE
	{
		NEW, RUNNING, WAITING, FINISHED;
	}
	
	public static void main(String [] args)
	{
		//Key as ENUM STATE and Having value as String.
		EnumMap<STATE, String> enumMapDemo = new EnumMap<>(STATE.class);
		enumMapDemo.put(STATE.NEW,"Task is New");
		enumMapDemo.put(STATE.WAITING, "Task is Waiting");
		enumMapDemo.put(STATE.RUNNING, "Task is Running");
		enumMapDemo.put(STATE.FINISHED, "Task is Finished");
		
		//Value Printed According to the Order of Enums.
		System.out.println(enumMapDemo);
		
		System.out.println("Size of EnumMap : "+enumMapDemo.size());
		
		Iterator<STATE> iterator = enumMapDemo.keySet().iterator();
		while(iterator.hasNext())
		{
			System.out.println("Values : "+enumMapDemo.get(iterator.next()));
		}
	}
}
