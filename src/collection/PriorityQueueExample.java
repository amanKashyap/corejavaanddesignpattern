package collection;

import java.util.Iterator;
import java.util.PriorityQueue;

public class PriorityQueueExample 
{
	public static void main(String[] args) 
	{
		PriorityQueue<Integer> queue = new PriorityQueue<>();
		queue.add(15);
		queue.add(10);
		queue.add(7);
		queue.add(35);
		queue.add(25);
		queue.add(1);
		queue.add(12);
		//Raised Runtime Error : NPE
		/*queue.add(null);*/
		
		Iterator<Integer> itr = queue.iterator();
		while(itr.hasNext())
		{
			System.out.println("Value: "+itr.next());
		}
		
		System.out.println("Priority Queue :"+queue);

	}

}
