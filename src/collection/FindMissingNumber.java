package collection;

import java.util.ArrayList;
import java.util.List;

public class FindMissingNumber 
{
	public static void main(String[] args) 
	{
		//Req: Find missing number from a list of 1-10.
		
		List<Integer> providedList = new ArrayList<Integer>();
		for(int i= 1 ; i <= 50; i++)
		{
			providedList.add(i);
		}
		for(int i= 52; i<=100 ; i++)
		{
			providedList.add(i);
		}
		
		System.out.println("Missing Number is :"+ FindMissingNumber.missingNumber(providedList, 100));
	}
	
	public static Integer missingNumber(List<Integer> providedList, Integer endPoint)
	{
		Integer totalSum = (endPoint*(endPoint+1))/2;
		
		for(Integer i : providedList)
		{
			totalSum = totalSum-i;
		}
		
		return totalSum;
	}

}
