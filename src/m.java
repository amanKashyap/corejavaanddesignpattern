
public class m{
	public static void main(String[] args) {
		n.getInstance();
	}
}
class n 
{
	private static n instance;
	
	private n()
	{
		
	}
	
	public static n getInstance()
	{
		synchronized (n.class) 
		{
			if(instance==null)
			{
				instance = new n();
			}
		}
		return instance;
	}
	
}

