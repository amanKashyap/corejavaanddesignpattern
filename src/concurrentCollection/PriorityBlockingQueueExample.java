/* 
 * Ref: http://www.baeldung.com/java-priority-blocking-queue
 * */

package concurrentCollection;

import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
		PriorityBlockingQueue<Integer> pbq = new PriorityBlockingQueue<>();
		
		ArrayList<Integer> addingElements = new ArrayList<>();
		addingElements.add(0);
		addingElements.add(1);
		addingElements.add(8);
		addingElements.add(5);
		addingElements.add(1);
		
		
		ArrayList<Integer> polledElements = new ArrayList<>();
		
		pbq.add(0);
		pbq.add(1);
		pbq.add(2);
		pbq.add(3);
		pbq.add(4);
		pbq.add(0);
		
		System.out.println("Priority Blocking Queue :"+pbq);
		pbq.drainTo(polledElements);
		System.out.println("Drained Array List :"+polledElements);
		
		
		new Thread(() -> {
							System.out.println("Polling Started");
							Integer polledElement;
							while(true)
							{
								try 
								{
									polledElement = pbq.take();
									System.out.println("Polled Element : "+ polledElement);
								} 
								catch (InterruptedException e) 
								{							
									e.printStackTrace();
								}	
							}
						 }).start();
		
		Thread.sleep(1000);
		/*
		 * Polling action was blocked till an element was added into the Queue.
		 */
		System.out.println("Adding Element to Priority Blocking Queue");
		pbq.add(9);
		
		pbq.addAll(addingElements);
		System.out.println("Priority Blocking Queue Before Sleep:"+pbq);
		Thread.sleep(1000);
		System.out.println("Priority Blocking Queue :"+pbq);
		
	}

}
