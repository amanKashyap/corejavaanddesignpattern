package concurrentCollection;


import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapExample {

	public static void main(String[] args) 
	{
		Map<Integer, String> concurrentHashMap = new ConcurrentHashMap<Integer, String>();
		
		for(int i =0 ; i<5 ; i++)
		{
			concurrentHashMap.put(i,"Test"+i);
		}
		System.out.println("Concurrent HashMap :"+concurrentHashMap);
		
		Iterator<Integer> keySet = concurrentHashMap.keySet().iterator();
		
		while(keySet.hasNext())
		{
			Integer key = keySet.next();
			System.out.println("Before Key :"+ key);
			System.out.println("Before Value :"+concurrentHashMap.get(key));
			if(key%2 == 0)
			{
				concurrentHashMap.remove(key);
			}
			System.out.println("After Key :"+ key);
			System.out.println("After Value :"+concurrentHashMap.get(key));
			
		}

	}

}
