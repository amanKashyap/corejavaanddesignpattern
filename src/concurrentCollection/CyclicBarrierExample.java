package concurrentCollection;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample 
{
	public static void main(String[] args) 
	{
		Runnable barrierAction1 = () -> {
										 System.out.println("Barrier Action1 Crossed");
									   };
		
		Runnable barrierAction2 = () -> {
					 					  System.out.println("Barrier Action2 Crossed");
										};
		
		/*
		 * Barrier Waiting for 2 Threads.
		 * Having an action as barrierAction 1
		 */
		CyclicBarrier cyclicBarrier = new CyclicBarrier(2,barrierAction1);	
		CyclicBarrier cyclicBarrier2 = new CyclicBarrier(2,barrierAction2);
		
		new BarrierThread(cyclicBarrier, cyclicBarrier2).start();
		new BarrierThread(cyclicBarrier, cyclicBarrier2).start();
	}

}

class BarrierThread extends Thread
{
	CyclicBarrier cyclicBarrier1 = null;
	CyclicBarrier cyclicBarrier2 = null;
	
	public BarrierThread(CyclicBarrier cyclicBarrier1, CyclicBarrier cyclicBarrier2) 
	{
		this.cyclicBarrier1 = cyclicBarrier1;
		this.cyclicBarrier2 = cyclicBarrier2;
	}
	
	@Override
	public void run()
	{
		 System.out.println(Thread.currentThread().getName()+" Started");
		 try 
		 {
			Thread.sleep(1000);
			System.out.println(Thread.currentThread().getName()+" waiting a Barrier 1");
		    this.cyclicBarrier1.await();
			 
			Thread.sleep(1000);
			System.out.println(Thread.currentThread().getName()+" waiting a Barrier 2");
			this.cyclicBarrier2.await();
			
			System.out.println(Thread.currentThread().getName()+" Completed");
		 } 		 
		 catch (InterruptedException | BrokenBarrierException e) 
		 {		
			e.printStackTrace();
		 }
	}
}


