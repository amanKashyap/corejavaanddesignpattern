package concurrentCollection;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SynchronousQueueExample 
{
	public static void main(String[] args) 
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		SynchronousQueue<Integer> syncQueue = new SynchronousQueue<>();
		
		Runnable producer = () -> {
									//Set A Random ThreadLocal Value into produceRandomElement
									Integer produceRandomElement = ThreadLocalRandom.current().nextInt();
									try
									{
										System.out.println("Produce Element :"+produceRandomElement);
										syncQueue.put(produceRandomElement);
									}
									catch(InterruptedException e)
									{
										e.printStackTrace();
									}									
								  };
								  
        Runnable consumer = () -> {
        							try
        							{
        								Integer consumeElement = syncQueue.take();
        								System.out.println("Consume Element :"+consumeElement);
        							}
        							catch(InterruptedException e)
        							{
        								e.printStackTrace();
        							}
        						  };
        						  
       
        executorService.execute(consumer);
        executorService.execute(producer);
        
        executorService.shutdown();
         
	}

}
