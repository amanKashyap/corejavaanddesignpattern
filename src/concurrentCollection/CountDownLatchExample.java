package concurrentCollection;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchExample 
{
	public static void main(String[] args) 
	{
		CountDownLatch latch = new CountDownLatch(3);
		
		WaitingThread waitingThread = new WaitingThread(latch);
		DecrementerThread decrementerThread = new DecrementerThread(latch);
		
		new Thread(decrementerThread).start();
		new Thread(waitingThread).start();

	}

}

class WaitingThread implements Runnable
{
	CountDownLatch latch = null;
	
	public WaitingThread(CountDownLatch latch)	
	{
		this.latch = latch;
	}
	
	@Override
	public void run() 
	{
		System.out.println("Waiting Thread Started");		
		try
		{
			latch.await();
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		System.out.println("Waiter Thread Released/Completed");
	}	
}


class DecrementerThread implements Runnable
{

	CountDownLatch latch = null;
	
	public DecrementerThread(CountDownLatch latch) 
	{
		this.latch = latch;
	}
	
	@Override
	public void run() 
	{
		System.out.println("Decrementer Thread started");
		try
		{
			Thread.sleep(1000);
			System.out.println("CountDown Latch 1");			
			latch.countDown();
			
			Thread.sleep(1000);
			System.out.println("CountDown Latch 2");			
			latch.countDown();
			
			Thread.sleep(1000);
			System.out.println("CountDown Latch 3");			
			latch.countDown();
			
			System.out.println("Decrementer Thread Ended");
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}		
	}
	
}