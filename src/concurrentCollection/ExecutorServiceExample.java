package concurrentCollection;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceExample 
{
	public static void main(String[] args) throws InterruptedException, ExecutionException 
	{
		Runnable runnableTask = () -> {
										System.out.println("Runnable Task Started");
										try
										{
											Thread.sleep(500);
										}
										catch(InterruptedException e)
										{
											e.printStackTrace();
										}
										System.out.println("Runnable Task Completed");
									  };
		
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		//execute() is used to Run Runnable Task.
		executorService.execute(runnableTask);
		
		Callable<String> callableTask = () -> {
												System.out.println("Callable Task Started");
												try
												{
													Thread.sleep(500);
												}
												catch(InterruptedException e)
												{
													e.printStackTrace();
												}
												System.out.println("Callable Task Completed");
												return "Callable Task Returned";
											  };
		
		
		//Callable Task Can be executed using submit()
		Future<String> futureObject = executorService.submit(callableTask);
		Thread.sleep(1500);
		System.out.println("Future Object : "+ futureObject.get());

	}

}
