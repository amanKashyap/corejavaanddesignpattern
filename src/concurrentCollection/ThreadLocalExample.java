package concurrentCollection;

public class ThreadLocalExample 
{
	public static void main(String[] args) throws InterruptedException 
	{
		ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
		
		Runnable runnable = () ->
								{
									threadLocal.set((int) (Math.random() * 100D));
									try
									{
										Thread.sleep(1000);
									}
									catch(InterruptedException e)
									{
										e.printStackTrace();
									}
									System.out.println("ThreadLocal value :"+threadLocal.get());
								};
		Thread thread1 = new Thread(runnable, "Thread1");
		Thread thread2 = new Thread(runnable, "Thread2");
		
		thread1.start();
		thread2.start();
		
		thread1.join();
		thread2.join();
	}

}

