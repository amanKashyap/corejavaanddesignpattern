package concurrentCollection;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class LinkedBlockingDequeExample 
{
	public static void main(String[] args)
	{
		/*
		 * Insertion Order
		 * Duplicates Allowed
		 */
		LinkedBlockingDeque<Integer> linkedBlockingQueue = new LinkedBlockingDeque<>();
		ExecutorService service = Executors.newFixedThreadPool(3);
		
		Runnable producer = () -> {
									System.out.println("Producer Thread");
									try
									{
										Thread.sleep(1000);
									}
									catch(InterruptedException e)
									{
										e.printStackTrace();
									}
									linkedBlockingQueue.addFirst(1);
									linkedBlockingQueue.addLast(45);
									linkedBlockingQueue.add(2);
									linkedBlockingQueue.add(4);
									linkedBlockingQueue.add(2);
									System.out.println("Linked Blocking Deque :"+linkedBlockingQueue);
								  };
								  
		Runnable consume = () -> {
									System.out.println("Consumer Thread");																	
									try 
									{										
										System.out.println("Take First: "+linkedBlockingQueue.takeFirst());
										System.out.println("Take Last: "+linkedBlockingQueue.takeLast());
										
									} 									
									catch (InterruptedException e) 
									{									
										e.printStackTrace();
									}
								 };
		
		service.execute(consume);
		service.execute(producer);
								 
	}
}
