package concurrentCollection;

import java.text.AttributedCharacterIterator;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerAssignment 
{
	public static void main(String[] args) throws InterruptedException 
	{
		ProcessingThread runnable = new ProcessingThread();
		Thread thread1 = new Thread(runnable,"Thread1");
		Thread thread2 = new Thread(runnable,"Thread2");
		
		//Starting the Threads.
		thread1.start();
		thread2.start();
		
		thread1.join();
		thread2.join();
		
		System.out.println("Processing Done");
		System.out.println("Count = "+runnable.getCount());
		System.out.println("Atomic Count = "+runnable.getAtomicCount());

	}

}

class ProcessingThread implements Runnable
{
	private Integer count;
	private AtomicInteger atomicCount = new AtomicInteger(0);
	
	public Integer getCount()
	{
		return count;
	}
	
	public int getAtomicCount()
	{
		return atomicCount.get();
	}
	
	@Override
	public void run()
	{
		for(int i=0 ; i<5 ; i++)
		{
			processSomething();
			count++;
			//atomicCount.incrementAndGet();
		}
	}
	
	private void processSomething()
	{
		//Processing A Job
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
