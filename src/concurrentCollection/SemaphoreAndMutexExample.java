package concurrentCollection;

import java.net.StandardSocketOptions;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

//Semaphore in Java act as Counting Semaphore
public class SemaphoreAndMutexExample 
{
	public static void main(String[] args) 
	{
		//4 Permits are available.
		//Semaphore semaphore = new Semaphore(4);
		
		//Converting the Above Semaphore to Mutex.
		Semaphore semaphore = new Semaphore(1);
		WorkerThread worker = new WorkerThread(semaphore);
		
		Thread thread1 = new Thread(worker);
		Thread thread2 = new Thread(worker);
		Thread thread3 = new Thread(worker);
		Thread thread4 = new Thread(worker);
		Thread thread5 = new Thread(worker);
		Thread thread6 = new Thread(worker);
		Thread thread7 = new Thread(worker);
		
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();
		thread7.start();
	}
}

class WorkerThread implements Runnable
{

	Semaphore semaphore;
	
	public WorkerThread(Semaphore semaphore)	
	{
		this.semaphore = semaphore;
	}
	
	@Override
	public void run() 
	{
		System.out.println(Thread.currentThread().getName()+" trying to acquire lock.");
		System.out.println("Acquired by : "+Thread.currentThread().getName()+" "+semaphore.tryAcquire());
		
		try 
		{
			Thread.sleep(500);
			
			System.out.println("Available Permit : "+semaphore.availablePermits());
			System.out.println("Action Started");
			for(int i=0; i<2;i++)
			{
				System.out.println("Action "+(i+1));
			}
			System.out.println("Action Completed");
		} 
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			System.out.println("Releasing the Permit");
			semaphore.release();
		}
	}
	
}
