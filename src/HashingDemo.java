import java.util.HashSet;
import java.util.Iterator;

public class HashingDemo 
{
	public static void main(String[] args) 
	{
		Employee e1 = new Employee(1, "One");
		Employee e2 = new Employee(2, "Two");
		Employee e3 = new Employee(3, "Three");
		Employee e4 = new Employee(4, "Four");
		Employee e5 = new Employee(5, "Five");
		Employee e6 = new Employee(6, "Six");
		Employee e7 = new Employee(7, "Seven");
		Employee e8 = new Employee(8, "Eight");
		Employee e9 = new Employee(9, "Nine");
		Employee e10 = new Employee(10, "Ten");
		
		HashSet<Employee> hashSet = new HashSet<>();
		hashSet.add(e1);
		hashSet.add(e2);
		hashSet.add(e3);
		hashSet.add(e4);
		hashSet.add(e5);
		hashSet.add(e6);
		hashSet.add(e7);
		hashSet.add(e8);
		hashSet.add(e9);
		hashSet.add(e10);
		hashSet.add(null);
		
		System.out.println(hashSet);
		
		System.out.println("**********************************************");
		Iterator<Employee> itr = hashSet.iterator();
		while(itr.hasNext())
		{
			System.out.println("Element :"+itr.next());
		}
	}
}

class Employee
{
	private int empId;
	private String empName;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	@Override
	public String toString() 
	{
		return empId+"-"+empName;	
	}
	@Override
	public int hashCode() {
		/*final int prime = 31;
		int result = 1;
		result = prime * result + empId;
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		return result;*/
		
		return 1;
	}
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (empId != other.empId)
			return false;		
		return true;
	}
	public Employee(int empId, String string) {
		super();
		this.empId = empId;
		this.empName = string;
	}
	
}