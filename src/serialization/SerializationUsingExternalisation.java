package serialization;

import java.io.Externalizable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SerializationUsingExternalisation 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException 
	{
		ExternalDemo demo = new ExternalDemo("Test", 1, 2);
		FileOutputStream fos = new FileOutputStream("abcd.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		//Internally writeExternal is called
		oos.writeObject(demo);
		
		System.out.println("Object writen to File");
		System.out.println("Reading Object from file");
		
		
		FileInputStream fis = new FileInputStream("abcd.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		ExternalDemo deSerializedDemo = (ExternalDemo) ois.readObject();
		System.out.println("s :"+deSerializedDemo.s);
		System.out.println("i :"+deSerializedDemo.i);
		//Default value for j as it has not been serialized
		System.out.println("j :"+deSerializedDemo.j);
	}
}

class ExternalDemo implements Externalizable
{
	String s;
	int i;
	int j;
	
	//InvalidClassException Raised as No-Arg Constructor is not available.
	public ExternalDemo(String s, int i, int j) 
	{
		this.s =s;
		this.i =i;
		this.j =j;
	}
	
	public ExternalDemo() 
	{
		System.out.println("Added A No-Arg Constructor");
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException 
	{
		//Serializing only String s and int i and not int j.
		out.writeObject(s);
		out.writeInt(i);		
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException 
	{
		s = (String) in.readObject();
		i = in.readInt();		
	}
	
}
