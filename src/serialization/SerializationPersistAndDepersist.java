package serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationPersistAndDepersist 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException 
	{
		Student student = new Student();
		student.setStudentId(1);
		student.setStudentName("ABCD");
		
		FileOutputStream fos = new FileOutputStream("abc.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(student);
		
		oos.flush();
		/*
		 * File created in the below mentioned directory:
		 * C:\Users\aman.kashyap\workspace\Testing
		 */
		System.out.println("Sucessfully Persisted");
		
		FileInputStream fis = new FileInputStream("abc.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Student stu = (Student) ois.readObject();
		
		System.out.println("Sucessfully Depersisted");
		System.out.println("Student ID : " +stu.getStudentId());
		System.out.println("Student Name : " +stu.getStudentName());
		System.out.println("Student Address : "+stu.getStudentAddress());
	}

}

class Student implements Serializable
{
	/*
	 * If Student Name is volatile. Output ABCD
	 * If Student Name is transient. Output null
	 * If Student Name is static. Output ABCD
	 * If Student Name is transient static. Output ABCD
	 */
	private String studentName;
	private Integer studentId;
	/*
	 * If Student Address is final. Output XYZ i.e. directly value is persisted
	 * If Student Address is transient final. Output XYZ i.e. directly value is persisted
	 */
	private final String studentAddress = "Pune";
	
	public String getStudentAddress() {
		return studentAddress;
	}
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Integer getStudentId() {
		return studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	
}