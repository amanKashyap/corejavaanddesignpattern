package serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationWithInheritance 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException 
	{
		Employee employee = new Employee();
		employee.setEmployeeId(1);
		employee.setEmployeeName("ABCD");
		employee.setGender("Male");
		
		FileOutputStream fos = new FileOutputStream("xyz.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(employee);
		
		oos.flush();
		/*
		 * File created in the below mentioned directory:
		 * C:\Users\aman.kashyap\workspace\Testing
		 */
		System.out.println("Sucessfully Persisted");
		
		FileInputStream fis = new FileInputStream("xyz.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Employee emp = (Employee) ois.readObject();
		
		System.out.println("Sucessfully Depersisted");
		System.out.println("Employee ID : " +emp.getEmployeeId());
		System.out.println("Employee Name : " +emp.getEmployeeName());
		
		/*
		 * Value of Gender is not persisted as Person class does not implement 
		 * Serializable. 
		 */
		System.out.println("Employee Gender : "+emp.getGender());
	}
}

class Person
{
	private String gender;
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}	
}

class Employee extends Person implements Serializable
{
	private String employeeName;
	private int employeeId;
	
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}		
}