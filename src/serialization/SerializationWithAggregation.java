package serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationWithAggregation 
{
	public static void main(String[] args) throws IOException, ClassNotFoundException 
	{
		Address addr = new Address();
		addr.setCityName("Pune");
		addr.setStreetName("Hinjewadi");
		
		Citizen citizen = new Citizen();
		citizen.setCitizenId(1);
		citizen.setCitizenName("ABCDE");
		citizen.setCitizenAddress(addr);
		
		FileOutputStream fos = new FileOutputStream("QWE.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		//NotSerializableException because Address doesn't implement Serializable
		oos.writeObject(citizen);
		oos.flush();
		System.out.println("Citizen Persisted");
		
		FileInputStream fis = new FileInputStream("QWE.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Citizen cit = (Citizen)ois.readObject();
		
		System.out.println("Citizen Name : "+cit.getCitizenId());
		System.out.println("Citizen ID : "+cit.getCitizenId());
		System.out.println("Citizen StreetName : "+cit.getCitizenAddress().getStreetName());
		System.out.println("Citizen CityName : "+cit.getCitizenAddress().getCityName());
		

	}

}
class Citizen implements Serializable
{
	private Integer citizenId;
	private String citizenName;
	private Address citizenAddress;
	
	public Integer getCitizenId() {
		return citizenId;
	}
	public void setCitizenId(Integer citizenId) {
		this.citizenId = citizenId;
	}
	public String getCitizenName() {
		return citizenName;
	}
	public void setCitizenName(String citizenName) {
		this.citizenName = citizenName;
	}
	public Address getCitizenAddress() {
		return citizenAddress;
	}
	public void setCitizenAddress(Address citizenAddress) {
		this.citizenAddress = citizenAddress;
	}
	
	
}

class Address 
{
	private String streetName;
	private String cityName;
	
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
}